The exercise consists in creating a prototype of Booking Component. 
The Booking Component allows users to search for f​lights​ from an 
o​rigin​ to a d​estination ​in a given departing ​and​ return dates.

Please try to​ “copy” ​the more important features of the UI below. 
You can find more information in the Requirements section. Feel free 
to use any library or framework to create your prototype.

> Note: ​It’s not mandatory to reproduce the exact same copy of this UI, 
> only the parts you deem necessary for a user to book a flight.

![Booking](./booking.png)

> You can see all the behavior and extract responses data in the 
> following URL: https://www.kayak.com/

## Technical spec

The architecture will be split between a back-end and a web front-end, for
instance providing a JSON in/out RESTful API. Feel free to use any other
technologies provided that the general client/service architecture is
respected.

Choose **one** of the following technical tracks that best suits your skillset:

1. **Full-stack**: include both front-end and back-end.
1. **Back-end track**: include a minimal front-end (e.g. a static view or API
   docs). Write, document and test your API as if it will be used by other
   services.
1. **Front-end engineering track** include a minimal back-end, or use mocked data.
   Write, document and test your UI as it was production ready. Focus on good
   software design / architecture.
1. **Front-end design track**: include a minimal back-end, or use mocked data.
   Focus on making the interface as polished as possible. Ideally a single
   page app with a single `index.html` linking to external JS/CSS/etc. You may
   take this opportunity to demonstrate your CSS or HTML knowledge.

## Host it!

Ideally, when you’re done, host it somewhere (e.g. on Amazon EC2, render, Google AppEngine, Netlify, Vercel, etc.).

If that's not possible let us know why and give instructions on how to run it.
